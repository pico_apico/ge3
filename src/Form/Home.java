package Form;

import model.*;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.TemporalType;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import java.text.DateFormat;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;

/**
 *
 * @author kdoug
 * @author maria
 * @author elenisal
 */
public class Home extends javax.swing.JFrame {
    // Διαχείριση Οντοτήτων
    EntityManagerFactory emf = Persistence.createEntityManagerFactory("Covid19PU");
    EntityManager em = emf.createEntityManager();
    EntityTransaction tx = em.getTransaction();
    CountryJpaController cjc = new CountryJpaController(emf);
    CoviddataJpaController covjc = new CoviddataJpaController(emf);

    /**
     * Creates new form Home
     */
    public Home() {
        initComponents();        
        //Αρχικοποίηση μετρητή χωρών και δεδομένων που έχουν εισαχθεί
        List<Country> countries = cjc.findCountryEntities();
        int size1 = countries.size();
        jLabel11.setText(String.valueOf(size1));
        List<Coviddata> covid = covjc.findCoviddataEntities();
        int size2 = covid.size();
        jLabel14.setText(String.valueOf(size2));
        //Δημιουργία Model για τα Combobox και την list με δυνατότητα επιλογής χωρών
        DefaultComboBoxModel model1 = new DefaultComboBoxModel();
        //Διαπέραση χωρών και εισαγωγή στη λίστα ονομάτων χωρών 
        //μόνο τα ονόματα των χωρών στα σχετικά Drop Down μενού
        for (Country c : countries) {
            model1.addElement(c.getName());
        }
        jComboBox1.setModel(model1);
        jComboBox3.setModel(model1);
        jList1.setModel(model1);  
    }
        
    //Εισαγωγή δεδομένων CovidData.
    private void ImportCovidData (String CovidDataType) {
        String dataTypeValue="";
        String url="https://covid2019-api.herokuapp.com/timeseries/";
        int DataKind = 0;  //Τύπος δεδομένων CovidDtata 
        
        //Είδος δεδομένων (1 = Θάνατοι, 2 = Ασθενείς που έχουν ανακάμψει, 3 = Επιβεβαιωμένα κρούσματα)
        if (CovidDataType=="deaths") {
            DataKind=1;
            dataTypeValue="Θάνατοι";
        } 
        else if (CovidDataType=="recovered") {
            DataKind=2;
            dataTypeValue="Ασθενείς που έχουν ανακάμψει";
        }
        else if (CovidDataType=="confirmed") {
            DataKind=3;
            dataTypeValue="Επιβεβαιωμένα κρούσματα";
        }
        
        //Διάβασμα δεδομένων από API
        //Χρήση κώδικα άσκησης για κλήση HTTP  
        String urlToCall = url+CovidDataType;
        System.out.println(urlToCall);
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder().url(urlToCall).build();
        try (okhttp3.Response response = client.newCall(request).execute()) {
            if (response.isSuccessful() && response.body() != null) {
                String responseString = response.body().string();
                // Eισαγωγή των Json στοιχείων στην βάση
                GsonBuilder builder = new GsonBuilder();
                builder.setPrettyPrinting();
                Gson gson = builder.create();
                JsonObject json = gson.fromJson(responseString, JsonObject.class);
                JsonArray items = json.get(CovidDataType).getAsJsonArray();
                //Διατρέχουμε τη λίστα
                for (JsonElement element : items) {
                    JsonObject object = element.getAsJsonObject();
                    Set<Map.Entry<String, JsonElement>> entries = object.entrySet();
                    //Δεν εισάγουμε τις χώρες που έχουν providence/state
                    try {
                        if (element.getAsJsonObject().get("Province/State").getAsString().isEmpty()) {
                            //Εισαγγωγή της χώρας στον πίνακα COVIDDATA
                            String Name = object.getAsJsonObject().get("Country/Region").getAsString();
                            Query query1 = em.createQuery("SELECT c FROM Country c WHERE c.name = :name");
                            query1.setParameter("name", Name);
                            Country country = (Country) query1.getSingleResult();
                            //Δημιουργούμε ένα αντικείμενο Coviddata και καλούμε setter από την κλάση coviddata
                            Coviddata coviddata = new Coviddata();
                            coviddata.setCountry(country);
                            coviddata.setDatakind((short) DataKind);                          
                            //Διαπερνάμε τις εγγραφές
                            int quantity = 0;
                            
                            for (Map.Entry<String, JsonElement> entry : entries) {
                                //αν βρω ημερομηνία την καταχωρώ από το // και μετά
                                if (entry.getKey().charAt(1) == '/' || entry.getKey().charAt(2) == '/') {
                                    //Δημιουργούμε ένα αντικείμενο date και καλούμε setter από την κλάση coviddata
                                    Date date = new SimpleDateFormat("MM/dd/yy").parse(entry.getKey());
                                    coviddata.setTrndate(date);
                                    //Εισάγουμε το prodqty και καλούμε setter από την κλάση coviddata
                                    int proodqty = entry.getValue().getAsInt();
                                    coviddata.setProodqty(proodqty);
                                    //υπολογισμός qty                                        
                                    quantity = proodqty - quantity;
                                    coviddata.setQty(quantity);
                                    quantity = proodqty;
                                    //Κλήση του CountryJpaController για δημιουργία (γέμισμα) του πίνακα COVIDDATA
                                    covjc.create(coviddata);
                                }
                            }
                        }
                    } catch (Exception ex) {
                        System.out.println(ex);
                    }
                }
            }
            //Ενημέρωση του πεδίου πλήθος δεδομέων Covid που έχουν εισαχθεί και κατάλληλα μηνύματα
            List<Coviddata> covid = covjc.findCoviddataEntities();
            int size2 = covid.size();
            jLabel14.setText(String.valueOf(size2));
            // Αν έχουν ήδη καταχωρηθεί τα δεδομένας της επιλεχθήσας από τον χρήστη καρηγορίας
            JOptionPane.showMessageDialog(null, "Τα δεδομένα της κατηγορίας '" + dataTypeValue + "' έχουν καταχωρηθεί.\n"
                    , "Covid19-Stats", JOptionPane.INFORMATION_MESSAGE);
        } catch (Exception ex) {
            System.out.println(ex);
        }
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLayeredPane1 = new javax.swing.JLayeredPane();
        Home = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        Import = new javax.swing.JPanel();
        jLabel20 = new javax.swing.JLabel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jLabel12 = new javax.swing.JLabel();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jCheckBox1 = new javax.swing.JCheckBox();
        jCheckBox2 = new javax.swing.JCheckBox();
        jCheckBox3 = new javax.swing.JCheckBox();
        jLabel6 = new javax.swing.JLabel();
        Country = new javax.swing.JPanel();
        jLabel16 = new javax.swing.JLabel();
        jComboBox1 = new javax.swing.JComboBox<>();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jDateChooser1 = new com.toedter.calendar.JDateChooser();
        jDateChooser2 = new com.toedter.calendar.JDateChooser();
        jButton5 = new javax.swing.JButton();
        jTabbedPane2 = new javax.swing.JTabbedPane();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTable2 = new javax.swing.JTable();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTable3 = new javax.swing.JTable();
        jButton6 = new javax.swing.JButton();
        jButton7 = new javax.swing.JButton();
        jButton8 = new javax.swing.JButton();
        jButton9 = new javax.swing.JButton();
        jLabel21 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jComboBox5 = new javax.swing.JComboBox<>();
        Map = new javax.swing.JPanel();
        jLabel24 = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        jDateChooser3 = new com.toedter.calendar.JDateChooser();
        jLabel26 = new javax.swing.JLabel();
        jDateChooser4 = new com.toedter.calendar.JDateChooser();
        jLabel27 = new javax.swing.JLabel();
        jButton11 = new javax.swing.JButton();
        jComboBox3 = new javax.swing.JComboBox<>();
        jLabel28 = new javax.swing.JLabel();
        jLabel29 = new javax.swing.JLabel();
        jScrollPane4 = new javax.swing.JScrollPane();
        jList1 = new javax.swing.JList<>();
        jLabel22 = new javax.swing.JLabel();
        MainMenuBar = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        jMenuItem3 = new javax.swing.JMenuItem();
        jMenuItem4 = new javax.swing.JMenuItem();
        jMenu3 = new javax.swing.JMenu();
        jMenuItem5 = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Covid19 Stats");
        setPreferredSize(new java.awt.Dimension(1024, 730));
        setResizable(false);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLayeredPane1.setOpaque(true);
        jLayeredPane1.setLayout(new java.awt.CardLayout());

        Home.setBackground(new java.awt.Color(46, 46, 46));
        Home.setName(""); // NOI18N
        Home.setPreferredSize(new java.awt.Dimension(1024, 728));
        Home.setLayout(null);

        jLabel2.setFont(new java.awt.Font("Fira Sans", 0, 60)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(204, 204, 204));
        jLabel2.setText("Covid19 Stats");
        Home.add(jLabel2);
        jLabel2.setBounds(70, 280, 410, 77);

        jLabel3.setFont(new java.awt.Font("Fira Sans", 0, 14)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(204, 204, 204));
        jLabel3.setText("Μπενία Μαρία Θεοδοσία");
        Home.add(jLabel3);
        jLabel3.setBounds(80, 370, 170, 19);

        jLabel4.setFont(new java.awt.Font("Fira Sans", 0, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(204, 204, 204));
        jLabel4.setText("Ντούγκας Κωνσταντίνος");
        Home.add(jLabel4);
        jLabel4.setBounds(80, 390, 180, 19);

        jLabel5.setFont(new java.awt.Font("Fira Sans", 0, 14)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(204, 204, 204));
        jLabel5.setText("Σαλαμανδράνη Ελένη");
        Home.add(jLabel5);
        jLabel5.setBounds(80, 410, 180, 19);

        jLabel7.setForeground(new java.awt.Color(204, 204, 204));
        jLabel7.setText("ΕΛΛΗΝΙΚΟ ΑΝΟΙΧΤΟ ΠΑΝΕΠΙΣΤΗΜΙΟ 2020-2021");
        Home.add(jLabel7);
        jLabel7.setBounds(80, 260, 350, 16);

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/SARS-CoV-2.png"))); // NOI18N
        jLabel1.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        jLabel1.setMaximumSize(new java.awt.Dimension(600, 1966));
        jLabel1.setMinimumSize(new java.awt.Dimension(600, 600));
        jLabel1.setPreferredSize(new java.awt.Dimension(500, 500));
        Home.add(jLabel1);
        jLabel1.setBounds(432, 0, 588, 649);

        jLabel9.setForeground(new java.awt.Color(204, 204, 204));
        jLabel9.setText("ΠΛΗ24 - ΗΛΕ45");
        Home.add(jLabel9);
        jLabel9.setBounds(80, 450, 110, 16);

        jLabel10.setForeground(new java.awt.Color(204, 204, 204));
        jLabel10.setText("Υπεύθυνος Καθηγητής: Μανής Γεώργιος");
        Home.add(jLabel10);
        jLabel10.setBounds(80, 470, 240, 16);

        jLayeredPane1.add(Home, "card3");
        Home.getAccessibleContext().setAccessibleName("home");

        Import.setBackground(new java.awt.Color(46, 46, 46));
        Import.setLayout(null);

        jLabel20.setFont(new java.awt.Font("Fira Sans", 0, 24)); // NOI18N
        jLabel20.setForeground(new java.awt.Color(204, 204, 204));
        jLabel20.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel20.setText("Covid19 Stats: Διαχείριση δεδομένων");
        Import.add(jLabel20);
        jLabel20.setBounds(200, 50, 630, 40);

        jTabbedPane1.setMaximumSize(new java.awt.Dimension(200, 200));
        jTabbedPane1.setPreferredSize(new java.awt.Dimension(200, 200));

        jPanel1.setMaximumSize(new java.awt.Dimension(200, 200));
        jPanel1.setPreferredSize(new java.awt.Dimension(200, 200));
        jPanel1.setLayout(null);

        jLabel8.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        jLabel8.setText("Πλήθος χωρών που έχουν εισαχθεί");
        jPanel1.add(jLabel8);
        jLabel8.setBounds(170, 120, 310, 24);

        jLabel11.setFont(new java.awt.Font("Dialog", 0, 120)); // NOI18N
        jLabel11.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel11.setText("189");
        jLabel11.setName(""); // NOI18N
        jPanel1.add(jLabel11);
        jLabel11.setBounds(140, 170, 240, 120);
        jLabel11.getAccessibleContext().setAccessibleName("");

        jButton1.setText("Εισαγωγή");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton1);
        jButton1.setBounds(390, 180, 100, 25);

        jButton2.setText("Διαγραφή");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton2);
        jButton2.setBounds(390, 250, 100, 25);

        jTabbedPane1.addTab("Χώρες", jPanel1);

        jPanel2.setMaximumSize(new java.awt.Dimension(200, 200));
        jPanel2.setLayout(null);

        jLabel12.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        jLabel12.setText("Τύποι δεδομένων (επιλογή)");
        jPanel2.add(jLabel12);
        jLabel12.setBounds(170, 120, 310, 24);

        jButton3.setText("Εισαγωγή");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        jPanel2.add(jButton3);
        jButton3.setBounds(170, 300, 100, 25);

        jButton4.setText("Διαγραφή");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });
        jPanel2.add(jButton4);
        jButton4.setBounds(350, 300, 100, 25);

        jLabel13.setText("Πλήθος δεδομένων που έχουν εισαχθεί:");
        jPanel2.add(jLabel13);
        jLabel13.setBounds(170, 260, 230, 16);

        jLabel14.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        jLabel14.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel14.setText("250000");
        jPanel2.add(jLabel14);
        jLabel14.setBounds(400, 260, 50, 16);

        jCheckBox1.setText("Θάνατοι");
        jCheckBox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBox1ActionPerformed(evt);
            }
        });
        jPanel2.add(jCheckBox1);
        jCheckBox1.setBounds(170, 160, 73, 25);

        jCheckBox2.setText("Επιβεβαιωμένα κρούσματα");
        jPanel2.add(jCheckBox2);
        jCheckBox2.setBounds(170, 190, 177, 25);

        jCheckBox3.setText("Ασθενείς που έχουν ανακάμψει");
        jPanel2.add(jCheckBox3);
        jCheckBox3.setBounds(170, 220, 202, 25);

        jTabbedPane1.addTab("Covid Data", jPanel2);

        Import.add(jTabbedPane1);
        jTabbedPane1.setBounds(200, 140, 630, 420);
        jTabbedPane1.getAccessibleContext().setAccessibleName("Countries");
        jTabbedPane1.getAccessibleContext().setAccessibleDescription("");

        jLabel6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/SARS-CoV-2_dark.png"))); // NOI18N
        jLabel6.setFocusable(false);
        Import.add(jLabel6);
        jLabel6.setBounds(432, 0, 588, 650);

        jLayeredPane1.add(Import, "card2");
        Import.getAccessibleContext().setAccessibleName("import");

        Country.setBackground(new java.awt.Color(40, 46, 46));
        Country.setLayout(null);

        jLabel16.setForeground(new java.awt.Color(204, 204, 204));
        jLabel16.setText("Χώρα");
        Country.add(jLabel16);
        jLabel16.setBounds(120, 100, 60, 16);

        jComboBox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox1ActionPerformed(evt);
            }
        });
        Country.add(jComboBox1);
        jComboBox1.setBounds(120, 120, 180, 22);

        jLabel17.setForeground(new java.awt.Color(204, 204, 204));
        jLabel17.setText("Εύρος δεδομένων");
        Country.add(jLabel17);
        jLabel17.setBounds(340, 100, 120, 16);

        jLabel18.setForeground(new java.awt.Color(204, 204, 204));
        jLabel18.setText("Έως");
        Country.add(jLabel18);
        jLabel18.setBounds(570, 130, 30, 16);

        jLabel19.setForeground(new java.awt.Color(204, 204, 204));
        jLabel19.setText("Από");
        Country.add(jLabel19);
        jLabel19.setBounds(340, 130, 40, 16);
        Country.add(jDateChooser1);
        jDateChooser1.setBounds(380, 120, 150, 22);
        Country.add(jDateChooser2);
        jDateChooser2.setBounds(610, 120, 150, 22);

        jButton5.setText("ΕΦΑΡΜΟΓΗ");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });
        Country.add(jButton5);
        jButton5.setBounds(780, 120, 110, 25);

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Title1", "Title2", "Title3"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.setIntercellSpacing(new java.awt.Dimension(10, 10));
        jTable1.setRowHeight(25);
        jScrollPane1.setViewportView(jTable1);

        jTabbedPane2.addTab("ΘΑΝΑΤΟΙ", jScrollPane1);

        jTable2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Title1", "Title2", "Title3"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable2.setIntercellSpacing(new java.awt.Dimension(10, 10));
        jTable2.setRowHeight(25);
        jScrollPane2.setViewportView(jTable2);

        jTabbedPane2.addTab("ΕΠΙΒΕΒΑΙΩΜΕΝΑ", jScrollPane2);

        jTable3.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Title1", "Title2", "Title3"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable3.setIntercellSpacing(new java.awt.Dimension(10, 10));
        jTable3.setRowHeight(25);
        jScrollPane3.setViewportView(jTable3);

        jTabbedPane2.addTab("ΑΝΕΚΑΜΨΑΝ", jScrollPane3);

        Country.add(jTabbedPane2);
        jTabbedPane2.setBounds(120, 160, 770, 400);

        jButton6.setText("ΔΙΑΓΡΑΜΜΑ");
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });
        Country.add(jButton6);
        jButton6.setBounds(300, 580, 110, 25);

        jButton7.setText("ΧΑΡΤΗΣ");
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton7ActionPerformed(evt);
            }
        });
        Country.add(jButton7);
        jButton7.setBounds(430, 580, 100, 25);

        jButton8.setText("ΔΙΑΓΡΑΦΗ");
        jButton8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton8ActionPerformed(evt);
            }
        });
        Country.add(jButton8);
        jButton8.setBounds(642, 580, 100, 25);

        jButton9.setText("ΚΑΘΑΡΙΣΜΟΣ");
        jButton9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton9ActionPerformed(evt);
            }
        });
        Country.add(jButton9);
        jButton9.setBounds(770, 580, 120, 25);

        jLabel21.setFont(new java.awt.Font("Fira Sans", 0, 24)); // NOI18N
        jLabel21.setForeground(new java.awt.Color(204, 204, 204));
        jLabel21.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel21.setText("Covid19 Stats: Προβολή ανά χώρα");
        Country.add(jLabel21);
        jLabel21.setBounds(280, 40, 610, 40);

        jLabel15.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/SARS-CoV-2_dark.png"))); // NOI18N
        jLabel15.setFocusable(false);
        Country.add(jLabel15);
        jLabel15.setBounds(432, 0, 588, 649);

        jComboBox5.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Όλα", "Θάνατοι", "Επιβεβαιωμένα", "Ανέκαμψαν", "Σωρευτικά" }));
        jComboBox5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox5ActionPerformed(evt);
            }
        });
        Country.add(jComboBox5);
        jComboBox5.setBounds(120, 580, 160, 22);

        jLayeredPane1.add(Country, "card4");
        Country.getAccessibleContext().setAccessibleName("country");

        Map.setBackground(new java.awt.Color(40, 46, 46));
        Map.setLayout(null);

        jLabel24.setForeground(new java.awt.Color(204, 204, 204));
        jLabel24.setText("Εύρος δεδομένων");
        Map.add(jLabel24);
        jLabel24.setBounds(340, 100, 140, 16);

        jLabel25.setForeground(new java.awt.Color(204, 204, 204));
        jLabel25.setText("Από");
        Map.add(jLabel25);
        jLabel25.setBounds(340, 130, 40, 16);
        Map.add(jDateChooser3);
        jDateChooser3.setBounds(380, 120, 150, 22);

        jLabel26.setForeground(new java.awt.Color(204, 204, 204));
        jLabel26.setText("Έως");
        Map.add(jLabel26);
        jLabel26.setBounds(570, 130, 30, 16);
        Map.add(jDateChooser4);
        jDateChooser4.setBounds(610, 120, 150, 22);

        jLabel27.setFont(new java.awt.Font("Fira Sans", 0, 24)); // NOI18N
        jLabel27.setForeground(new java.awt.Color(204, 204, 204));
        jLabel27.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel27.setText("Covid19 Stats: Προβολή χάρτη");
        Map.add(jLabel27);
        jLabel27.setBounds(120, 40, 770, 40);

        jButton11.setText("ΕΦΑΡΜΟΓΗ");
        jButton11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton11ActionPerformed(evt);
            }
        });
        Map.add(jButton11);
        jButton11.setBounds(780, 120, 110, 25);

        jComboBox3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox3ActionPerformed(evt);
            }
        });
        Map.add(jComboBox3);
        jComboBox3.setBounds(120, 120, 180, 22);

        jLabel28.setForeground(new java.awt.Color(204, 204, 204));
        jLabel28.setText("Βασική Χώρα");
        Map.add(jLabel28);
        jLabel28.setBounds(120, 100, 100, 16);

        jLabel29.setForeground(new java.awt.Color(204, 204, 204));
        jLabel29.setText("Χώρες. Εξατομικευμένη πολλαπλή επιλογή με Ctr + Click, Σειριακή πολλαπλή επιλογή με Click + Shift Click");
        Map.add(jLabel29);
        jLabel29.setBounds(120, 170, 770, 16);

        jList1.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        jScrollPane4.setViewportView(jList1);

        Map.add(jScrollPane4);
        jScrollPane4.setBounds(120, 190, 770, 410);

        jLabel22.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/SARS-CoV-2_dark.png"))); // NOI18N
        jLabel22.setFocusable(false);
        Map.add(jLabel22);
        jLabel22.setBounds(432, 0, 588, 649);

        jLayeredPane1.add(Map, "card5");
        Map.getAccessibleContext().setAccessibleName("map");

        getContentPane().add(jLayeredPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, 700));

        MainMenuBar.setBackground(new java.awt.Color(0, 0, 0));
        MainMenuBar.setForeground(new java.awt.Color(168, 168, 168));
        MainMenuBar.setToolTipText("Menu");
        MainMenuBar.setAlignmentX(1.0F);
        MainMenuBar.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        MainMenuBar.setInheritsPopupMenu(true);
        MainMenuBar.setMargin(new java.awt.Insets(10, 10, 10, 10));
        MainMenuBar.setMaximumSize(new java.awt.Dimension(140, 32769));
        MainMenuBar.setName(""); // NOI18N
        MainMenuBar.setNextFocusableComponent(jMenu1);
        MainMenuBar.setOpaque(false);
        MainMenuBar.setPreferredSize(new java.awt.Dimension(104, 40));

        jMenu1.setText("Αρχείο");
        jMenu1.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N

        jMenuItem1.setText("Εισαγωγή");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem1);

        jMenuItem2.setText("Έξοδος");
        jMenuItem2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                ClickedExit(evt);
            }
        });
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem2);

        MainMenuBar.add(jMenu1);

        jMenu2.setText("Προβολή");
        jMenu2.setToolTipText("Προβολή");
        jMenu2.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N

        jMenuItem3.setText("Δεδομένα Χώρας");
        jMenuItem3.setToolTipText("Δεδομένα Χώρας");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem3);

        jMenuItem4.setText("Δεδομένα σε Χάρτη");
        jMenuItem4.setToolTipText("Δεδομένα σε Χάρτη");
        jMenuItem4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem4ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem4);

        MainMenuBar.add(jMenu2);

        jMenu3.setText("Βοήθεια");
        jMenu3.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N

        jMenuItem5.setText("Σχετικά");
        jMenuItem5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem5ActionPerformed(evt);
            }
        });
        jMenu3.add(jMenuItem5);

        MainMenuBar.add(jMenu3);

        setJMenuBar(MainMenuBar);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void ClickedExit(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ClickedExit
        //πατώντας το X  η εφαρμογή κλείνει
        dispose();
        System.exit(0);
    }//GEN-LAST:event_ClickedExit

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        //Επιλέγοντας Εισαγωγή ενεργοποιείτε η φόρμα import για την εισαγωγή χωρών και δεδομένων
        Home.setVisible(false);
        Import.setVisible(true);
        Country.setVisible(false);
        Map.setVisible(false);
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
       //Επιλέγοντας Έξοδος η εφαρμογή κλείνει
       dispose();
       System.exit(0);
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void jComboBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox1ActionPerformed
        //Με το άνοιγμα της φόρμας είναι ήδη ενεργοποιημένο. Περιέχει λίστα χωρών 
    }//GEN-LAST:event_jComboBox1ActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        //Πατώντας το κουμπί ΕΜΦΆΝΙΣΗ
        // Έλεγχος για την σωστή επιλογή ημερομηνιών από τον χρήστη
        //Αν δεν έχει επιλέξει ημερομηνίες
        if (jDateChooser1.getDate() == null || jDateChooser2.getDate() == null) {
            JOptionPane.showMessageDialog(null, "Παρακαλώ επιλέξτε ημερομηνίες!", "Covid19-Stats Σφάλμα", JOptionPane.ERROR_MESSAGE);
            return;
        }
        //Αν η ημερομηνία αρχής είναι μεταγενέστερη της ημερομηνίας τέλους
        if (jDateChooser1.getDate().after(jDateChooser2.getDate())) {
            JOptionPane.showMessageDialog(null, "Παρακαλώ ελέγξτε τις ημερομηνίες!", "Covid19-Stats Σφάλμα", JOptionPane.ERROR_MESSAGE);
            return;
        }
        //Δημιουργία λίστας δεδομένων
        List<Coviddata> listdata = null;
        //Οι 3 πίνακες θα εμφανίζονται σε διαφορετικές καρτέλες
        DefaultTableModel model1 = new DefaultTableModel();
        DefaultTableModel model2 = new DefaultTableModel();
        DefaultTableModel model3 = new DefaultTableModel();
        //Ορίζουμε τις επικεφαλίδες στους πίνακες
        model1.setColumnIdentifiers(new String[]{"ΗΜΕΡΟΜΗΝΙΑ", "ΗΜΕΡΗΣΙΑ ΔΕΔΟΜΕΝΑ", "ΣΩΡΕΥΤΙΚΑ ΔΕΔΟΜΕΝΑ"});
        model2.setColumnIdentifiers(new String[]{"ΗΜΕΡΟΜΗΝΙΑ", "ΗΜΕΡΗΣΙΑ ΔΕΔΟΜΕΝΑ", "ΣΩΡΕΥΤΙΚΑ ΔΕΔΟΜΕΝΑ"});
        model3.setColumnIdentifiers(new String[]{"ΗΜΕΡΟΜΗΝΙΑ", "ΗΜΕΡΗΣΙΑ ΔΕΔΟΜΕΝΑ", "ΣΩΡΕΥΤΙΚΑ ΔΕΔΟΜΕΝΑ"});
        //Καταχωρούμε σε 2 string τις ημερομηνίες Από και Έως
        String startdate = jDateChooser1.getDate().toString();
        String enddate = jDateChooser2.getDate().toString();
        //Εμφανίζω τις χώρες από την Database
        List<Country> countries = cjc.findCountryEntities();
        // Επιλογή χώρας από τον χρήστη
        String userSelection = jComboBox1.getSelectedItem().toString();
        System.out.println(userSelection);
        Country select = new Country();
        //Βρίσκω την χώρα που επέλεξε ο χρήστης
        for (Country c : countries) {
            if (userSelection.equals(c.getName())) {
                select = new Country(c.getCountry());
            }
        }
        //Βρίσκω τα στοιχεία Coviddata της χώρας
        Query query = em.createQuery("SELECT c FROM Coviddata c WHERE c.country = :country");
        query.setParameter("country", select);
        listdata = query.getResultList();
        try {
            //Μετατρέπουμε τα String σε Date
            SimpleDateFormat start = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy", Locale.ENGLISH);
            SimpleDateFormat end = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy", Locale.ENGLISH);
            Date startdate1 = start.parse(startdate);
            Date enddate1 = end.parse(enddate);
            //Διόρθωση ώστε να ξεκινάει ο πίνακας από την ημερομηνία που επέλεξε ο χρήστης και όχι από την επόμενη
            LocalDateTime ldt = LocalDateTime.ofInstant(startdate1.toInstant(), ZoneId.systemDefault()).minusDays(1);
            startdate1 = Date.from(ldt.atZone(ZoneId.systemDefault()).toInstant());
            //Διαπερνάμε τις εγγραφές
            for (int i = 0; i < listdata.size(); i++) {
                //Πίνακας με θανάτους
                //Περιορίζουμε το εύρος των ημερομηνιών
                 if (listdata.get(i).getDatakind() == 1
                        && (listdata.get(i).getTrndate()).after(startdate1)
                        && (listdata.get(i).getTrndate()).before(enddate1)) {
                    //Γεμίζουμε τον πίνακα (το μοντέλο) γραμμή-γραμμή με date - qty - prodqty
                    Date date = listdata.get(i).getTrndate();
                    // Αλλάζουμε την μορφή της ημερομηνίας σε ημέρα-μήνας-έτος για αισθητικούς λόγους
                    DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                    String strDate = dateFormat.format(date);
                    String qty = String.valueOf(listdata.get(i).getQty());
                    String proodqty = String.valueOf(listdata.get(i).getProodqty());
                    model1.addRow(new String[]{strDate, qty, proodqty});
                }
                 //Πίνακας με επιβεβαιωμένα κρούσματα
                 //Περιορίζουμε το εύρος των ημερομηνιών
                if (listdata.get(i).getDatakind() == 3
                        && (listdata.get(i).getTrndate()).after(startdate1)
                        && (listdata.get(i).getTrndate()).before(enddate1)) {
                    //Γεμίζουμε τον πίνακα (το μοντέλο) γραμμή-γραμμή με date - qty - prodqty
                    Date date = listdata.get(i).getTrndate();
                    // Αλλάζουμε την μορφή της ημερομηνίας σε ημέρα-μήνας-έτος για αισθητικούς λόγους
                    DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                    String strDate = dateFormat.format(date);
                    String qty = String.valueOf(listdata.get(i).getQty());
                    String proodqty = String.valueOf(listdata.get(i).getProodqty());
                    model2.addRow(new String[]{strDate, qty, proodqty});
                }
                //Πίνακας με ασθενείς που ανέκαμψαν
                //Περιορίζουμε το εύρος των ημερομηνιών
                if (listdata.get(i).getDatakind() == 2
                        && (listdata.get(i).getTrndate()).after(startdate1)
                        && (listdata.get(i).getTrndate()).before(enddate1)) {
                    //Γεμίζουμε τον πίνακα (το μοντέλο) γραμμή-γραμμή με date - qty - prodqty
                    Date date = listdata.get(i).getTrndate();
                    // Αλλάζουμε την μορφή της ημερομηνίας σε ημέρα-μήνας-έτος για αισθητικούς λόγους
                    DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                    String strDate = dateFormat.format(date);
                    String qty = String.valueOf(listdata.get(i).getQty());
                    String proodqty = String.valueOf(listdata.get(i).getProodqty());
                    model3.addRow(new String[]{strDate, qty, proodqty});
                }
            }
        } catch (Exception ex) {
            System.out.println(ex);
        }
        //Στοιχίζουμε τα δεδομένα των πινάκκων στο κέντρο για αισθητικούς λόγους
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(javax.swing.JLabel.CENTER);
        jTable1.setDefaultRenderer(Object.class, centerRenderer);
        jTable2.setDefaultRenderer(Object.class, centerRenderer);
        jTable3.setDefaultRenderer(Object.class, centerRenderer);
        //Ενημερώνουμε τα jTable 
        jTable1.setModel(model1);
        jTable2.setModel(model2);
        jTable3.setModel(model3);
    }//GEN-LAST:event_jButton5ActionPerformed

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        //Επιλογή δημιουργία διαγράμματος
        //Επιλογή χώρας
        String selectedCountry = (String) jComboBox1.getSelectedItem();
        //Επιλογή εύρους ημερομηνιών
        Date startDate = jDateChooser1.getDate();
        Date endDate = jDateChooser2.getDate();
        Query query1 = em.createQuery("SELECT c FROM Coviddata c WHERE c.country.name = :selCountry AND c.trndate BETWEEN :startDate AND :endDate");
        query1.setParameter("selCountry", selectedCountry);
        query1.setParameter("startDate", startDate);
        query1.setParameter("endDate", endDate);
        //Ορισμός λίστας με δεδομένα CovidData
        List<Coviddata> covData = query1.getResultList();
        //Αποθηκεύω την επιλογή χρήστη για την κατηγορία δεδομένων (όλα,θάνατοι, επιβεβαιωμένα, ανέκαμψαν, σωρευτικά)
        int categoryselected = jComboBox5.getSelectedIndex();
        final LineChart1 chart = new LineChart1(selectedCountry + " Covid19 data", covData, categoryselected);
        chart.pack();
         //Εμφανίζουμε την φόρμα LineChart1
        chart.setVisible(true);

    }//GEN-LAST:event_jButton6ActionPerformed

    private void jComboBox3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox3ActionPerformed
         //Με το άνοιγμα της φόρμας είναι ήδη ενεργοποιημένο. Περιέχει λίστα χωρών  
    }//GEN-LAST:event_jComboBox3ActionPerformed

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        //Επιλέγοντας Δεδομένα Χώρας ενεργοποιείτε η φόρμα Country για την εμφάνιση δεδομένων χώρας
        Home.setVisible(false);
        Import.setVisible(false);
        Country.setVisible(true);
        Map.setVisible(false);
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void jMenuItem4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem4ActionPerformed
        //Επιλέγοντας Δεδομένα σε χάρτη ενεργοποιείτε η φόρμα Map για την εμφάνιση δεδομένων σε χάρτη
        Home.setVisible(false);
        Import.setVisible(false);
        Country.setVisible(false);
        Map.setVisible(true);
    }//GEN-LAST:event_jMenuItem4ActionPerformed

    private void jMenuItem5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem5ActionPerformed
        //Επιλέγοντας Σχετικά εμφανίζεται η αρχική φόρμα με πληροφορίες σχετικά με την εργασία
        Home.setVisible(true);
        Import.setVisible(false);
        Country.setVisible(false);
        Map.setVisible(false);
    }//GEN-LAST:event_jMenuItem5ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        //Εισαγωγή χωρών
        String category[] = {"deaths", "recovered", "confirmed", };
        //Διάβασμα δεδομένων από ΑΡΙ
        //Χρήση κώδικα άσκησης για κλήση HTTP
        //Για την εισαγωγή των χωρών χρειαζόμαστε μόνο μια κατηγορία έστω τους θανάτους
        String urlToCall = "https://covid2019-api.herokuapp.com/timeseries/" + category[0];
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder().url(urlToCall).build();
        try (okhttp3.Response response = client.newCall(request).execute()) {
            if (response.isSuccessful() && response.body() != null) {
                String responseString = response.body().string();
                System.out.println(responseString);
                //Eισαγωγή των Json στοιχείων στην βάση
                GsonBuilder builder = new GsonBuilder();
                builder.setPrettyPrinting();
                Gson gson = builder.create();
                JsonObject json = gson.fromJson(responseString, JsonObject.class);
                JsonArray items = json.get(category[0]).getAsJsonArray();
                //Διατρέχουμε τη λίστα 
                for (JsonElement element : items) {
                    try {
                        if (element.getAsJsonObject().get("Province/State").getAsString().isEmpty()) {
                            //Τα object που θέλουμε να εισαχθούν
                            String Name = element.getAsJsonObject().get("Country/Region").getAsString();
                            Double Lat = element.getAsJsonObject().get("Lat").getAsDouble();
                            Double Long = element.getAsJsonObject().get("Long").getAsDouble();
                            //Δημιουργία νέου αντικειμένου τύπου Country και κλήση των setters από την κλάση Country
                            Country country = new Country();
                            country.setName(Name);
                            country.setLat(Lat);
                            country.setLong1(Long);
                            //Κλήση του CountryJpaController για δημιουργία (γέμισμα) του πίνακα COUNTRY 
                            cjc.create(country);
                        }
                    } catch (Exception ex) {
                        System.out.println(ex);
                    }
                }
                //Ενημέρωση του πεδίου πλήθος χωρών που έχουν εισαχθεί και κατάλληλα μηνύματα στον χρήστη
                List<Country> countries = cjc.findCountryEntities();
                int size1 = countries.size();
                jLabel11.setText(String.valueOf(size1));
                //Αν οι χώρες έχουν ήδη καταxωριθεί θα εμφανίζεται κατάλληλο μήνυμα στον χρήστη
                Query query1 = em.createQuery("SELECT country FROM Country country");
                //αν η λίστα έχει μέγεθος > 0 δηλαδή έχουν εισαχθεί στοιχεία
                if (query1.getResultList().size() > 0) {
                    JOptionPane.showMessageDialog(null, "Όλες οι χώρες έχουν  καταχωριθεί", "Covid19-Stats", JOptionPane.INFORMATION_MESSAGE);
                }
            }
        } catch (Exception ex) {
            System.out.println(ex);
        }
        //Ενημέρωση combobox 
        List<Country> countries = cjc.findCountryEntities();    
       //Δημιουργία Model για τα Combobox και την list με δυνατότητα επιλογής χωρών
        DefaultComboBoxModel model1 = new DefaultComboBoxModel();
        //Διαπέραση χωρών και εισαγωγή στη λίστα ονομάτων χωρών 
        //μόνο τα ονόματα των χωρών στα σχετικά Drop Down μενού
        for (Country c : countries) {
        model1.addElement(c.getName());
       }
        jComboBox1.setModel(model1);
        jComboBox3.setModel(model1);
        jList1.setModel(model1);
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        //Διαγραφή όλων των χωρών (πρέπει να έχει προηγηθεί η διαγραφή των δεδομένων
        int question = JOptionPane.showConfirmDialog(null, "Θα διαγραφούν όλες οι χώρες!\n"
                + "Είστε βέβαιοι;", "Covid19-Stats", JOptionPane.YES_NO_OPTION);
        if (question == JOptionPane.YES_OPTION) {
            try {
                // Διαγραφή πίνακα COUNTRY
                tx.begin();
                //εάν υπάρχουν χώρες στη βάση  
                Query query1 = em.createQuery("SELECT c FROM Coviddata c");
                if (query1.getResultList().isEmpty()) {
                    em.createQuery("DELETE FROM Country").executeUpdate();
                    em.createNativeQuery("ALTER TABLE COUNTRY ALTER COLUMN COUNTRY RESTART WITH 1").executeUpdate();
                    tx.commit();
                    JOptionPane.showMessageDialog(null, "Οι χώρες διαγράφηκαν", "Covid19-Stats",
                            JOptionPane.INFORMATION_MESSAGE);
                } else {
                    JOptionPane.showMessageDialog(null, "Πρέπει πρώτα να διαγραφούν τα δεδομένα\n"
                            + "Επιλέξτε πρώτα την ΔΙΑΓΡΑΦΗ Covid Data", "Covid19-Stats", JOptionPane.INFORMATION_MESSAGE);
                }

            } catch (Exception ex) {
                System.out.println(ex);
            }
            //Ενημέρωση του πεδίου πλήθος χωρών που έχουν εισαχθεί
            List<Country> countries = cjc.findCountryEntities();
            int size1 = countries.size();
            jLabel11.setText(String.valueOf(size1));
        }
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        //Ελέγχεται αν έχει γίνει εισαγωγή χωρών
        Query query = em.createQuery("SELECT country FROM Country country");
        //αν η λίστα χωρών έχει μέγεθος  0 δηλαδή  δεν έχουν εισαχθεί χώρες
        if (query.getResultList().size() == 0) {
            JOptionPane.showMessageDialog(null, "Παρακαλώ εισάγετε πρώτα τις χώρες", "Covid19-Stats", JOptionPane.INFORMATION_MESSAGE);
        } else {            
            // Ειαγωγή δεδομένων
            //Εισαγωγή δεδομένων θανάτων
            if (jCheckBox1.isSelected()) {
                ImportCovidData("deaths");
            }
            //Εισαγωγή δεδομένων επιβεβαιωμένων κρουσμάτων
            if (jCheckBox2.isSelected()) {
                ImportCovidData("confirmed");
            }
            //Εισαγωγή δεδομένων ασθενών που έχουν ανακάμψει
            if (jCheckBox3.isSelected()) {
                ImportCovidData("recovered");
            }
        }
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        //Διαγραφή δεδομένων
        int question = JOptionPane.showConfirmDialog(null, "Θα διαγραφούν όλα τα δεδομένα! \n"
                + "\n Είστε βέβαιοι;", "Covid19-Stats", JOptionPane.YES_NO_OPTION);
        if (question == JOptionPane.YES_OPTION) {
            try {
                // Διαγραφή πίνακα COVIDDATA
                tx.begin();
                em.createQuery("DELETE FROM Coviddata").executeUpdate();
                em.createNativeQuery("ALTER TABLE COVIDDATA ALTER COLUMN COVIDDATA RESTART WITH 1").executeUpdate();
                tx.commit();
                JOptionPane.showMessageDialog(null, "Τα δεδομένα διαγράφηκαν", "Covid19_Stats",
                        JOptionPane.INFORMATION_MESSAGE);
            } catch (Exception ex) {
                System.out.println(ex);
            }
            //Ενημέρωση του πεδίου πλήθος δεδομένων που έχουν εισαχθεί
            List<Coviddata> covid = covjc.findCoviddataEntities();
            int size2 = covid.size();
            jLabel14.setText(String.valueOf(size2));
            jCheckBox1.setEnabled(true);
            jCheckBox2.setEnabled(true);
            jCheckBox3.setEnabled(true);
        }
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton9ActionPerformed
         // Με το πάτημα του ΚΑΘΑΡΙΣΜΟΣ καθαρίζουν οι πίνακες δεν γίνεται διαγραφή
        ((DefaultTableModel) jTable1.getModel()).setNumRows(0);
        ((DefaultTableModel) jTable2.getModel()).setNumRows(0);
        ((DefaultTableModel) jTable3.getModel()).setNumRows(0);
    }//GEN-LAST:event_jButton9ActionPerformed

    private void jButton8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton8ActionPerformed
        //Διαγραφή δεδομένων επιλεγμένης χώρας
        //Αποθηκεύουμε την χώρα που επέλεξε ο χρήστης να διαγράψει
        String selectedCountry = (String) jComboBox1.getSelectedItem();
        //Επιβεβαίωση διαγραφής με κατάλληλα μηνύματα
        int answer = JOptionPane.showConfirmDialog(null, "Είστε βέβαιοι οτι θέλετε να διαγράψετε τα δεδομένα της χώρας " + selectedCountry + ";", "", JOptionPane.YES_NO_OPTION);
        if (answer == JOptionPane.YES_OPTION) {
            try {
                tx.begin();
                Query query1 = em.createQuery("DELETE FROM Coviddata c WHERE c.country.name = :selCountry");
                query1.setParameter("selCountry", selectedCountry);
                query1.executeUpdate();
                tx.commit();
            } catch (Exception ex) {
                System.out.println(ex);
            }
            JOptionPane.showMessageDialog(null, "Τα δεδομένα διαγράφηκαν", "Covid19-Stats",
                    JOptionPane.INFORMATION_MESSAGE);
        }
        // Για αισθητικούς λόγους με την διαγραφή καθαρίζει ο πίνακας με τα δεδομένα που διεγράφησαν
        ((DefaultTableModel) jTable1.getModel()).setNumRows(0);
        ((DefaultTableModel) jTable2.getModel()).setNumRows(0);
        ((DefaultTableModel) jTable3.getModel()).setNumRows(0);
        
        //Ενημέρωση του πεδίου πλήθος δεδομένων που έχουν εισαχθεί (προηγούμενη φόρμα import)
        List<Coviddata> covid = covjc.findCoviddataEntities();
        int size2 = covid.size();
        jLabel14.setText(String.valueOf(size2));
    }//GEN-LAST:event_jButton8ActionPerformed

    private void jComboBox5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox5ActionPerformed
        // Περιέχει λίστα με 5 πιθανές επιλογες του χρήστη για την κατασκευή διαγράμματος
        //όλα - θάνατοι- επιβεβαιωμένα κρούσματα - ασθενείς που ανέκαμψαν - σωρευτικά
    }//GEN-LAST:event_jComboBox5ActionPerformed

    private void jButton11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton11ActionPerformed
        // Πατώντας ΕΦΑΡΜΟΓΗ εμφανίζεται 
        //1. ο χάρτης με την χώρα και το εύρος ημερομηνιών που έχει ήδη επιλέξει ο χρήστης στην φόρμα Country
        //2. o χάρτης με τις επιλογές του χρήστη στην τρέχουσα φόρμα Map
        // Έλεγχος για την σωστή επιλογή ημερομηνιών από τον χρήστη
        //Αν δεν έχει επιλέξει ημερομηνίες
        if (jDateChooser3.getDate() == null || jDateChooser4.getDate() == null) {
            JOptionPane.showMessageDialog(null, "Παρακαλώ επιλέξτε ημερομηνίες!", "Covid19-Stats Σφάλμα", JOptionPane.ERROR_MESSAGE);
            return;
        }
        //Αν η ημερομηνία αρχής είναι μεταγενέστερη της ημερομηνίας τέλους
        if (jDateChooser3.getDate().after(jDateChooser4.getDate())) {
            JOptionPane.showMessageDialog(null, "Παρακαλώ ελέγξτε τις ημερομηνίες!", "Covid19-Stats Σφάλμα", JOptionPane.ERROR_MESSAGE);
            return;
        }
        List<String> selectedCountries = new ArrayList();
        // Eπιλογή μιας μόνο χώρας από το combobox
        if (jList1.getSelectedValuesList() != null && jList1.getSelectedValuesList().isEmpty()) {
            selectedCountries.add(0, jComboBox3.getSelectedItem().toString());
        } else {
            // Επιλογή βασικής χώρας από combobox και επιπλέον χωρών από τη list
            selectedCountries = jList1.getSelectedValuesList();
            selectedCountries.add(0, jComboBox3.getSelectedItem().toString());
        }
        //Κατασκευάζουμε ArrayList με τις χώρες
        ArrayList<Country> countries = new ArrayList<>();
        //Κατασκευάζουμε μία λίστα όπου θα καταχωρούνται τα δεδομένα Coviddata
        List<Coviddata> coviddata = new ArrayList<>();
        //Κατασκευάζουμε ένα ArrayList όπου θα καταχωρούνται τα δεδομένα Coviddata των επιλεγμένων χωρών και ευρούς ημερομηνιών
        ArrayList<Coviddata> covidDat = new ArrayList<>();
        for (int i = 0; i < selectedCountries.size(); i++) {
            //Αναζητούμε την επιλεγμένη χώρα στην Database και την προσθέτουμε στην ArrayList με τις χώρες 
            Query q1 = em.createQuery("SELECT c FROM Country c WHERE c.name = :name");
            q1.setParameter("name", selectedCountries.get(i));
            Country country = (Country) q1.getSingleResult();
            countries.add(country);
            //Καταχωρούμε σε 2 string τις ημερομηνίες Από και Έως
            String startdate = jDateChooser3.getDate().toString();
            String enddate = jDateChooser4.getDate().toString();
            SimpleDateFormat start = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy", Locale.ENGLISH);
            SimpleDateFormat end = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy", Locale.ENGLISH);
            Date date1 = null;
            Date date2 = null;
            //Μετατρέπουμε τα String σε date
            try {
                date1 = start.parse(startdate);
                date2 = end.parse(enddate);
            } catch (ParseException ex) {
                System.out.println(ex);
            }
            //Επιλέγουμε τα δεδομένα που βρίσκονται έντος του έυρους των επιλεγμένων ημερομηνιών και τα προσθέτω στον covidDat
            Query q2 = em.createQuery("SELECT c FROM Coviddata c WHERE c.country = :country AND c.trndate BETWEEN :startDate AND :endDate");
            q2.setParameter("country", country);
            q2.setParameter("startDate", date1, TemporalType.DATE);
            q2.setParameter("endDate", date2, TemporalType.DATE);
            //Καταχωρώ το αποτέλεσμα του Query στην List<Coviddata> coviddata  
            coviddata = q2.getResultList();
            covidDat.addAll(coviddata);
        }
        String list[][] = new String[countries.size()][3];
        for (int i = 0; i < countries.size(); i++) {
            int deaths = 0;
            int confirmed = 0;
            int recovered = 0;
            String pattern = "EEE dd/MM/yyyy";
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
            String date1 = simpleDateFormat.format(jDateChooser3.getDate());
            String date2 = simpleDateFormat.format(jDateChooser4.getDate());
            for (Coviddata data : covidDat) {
                if (countries.get(i).getName().equals(data.getCountry().getName())) {
                    if (data.getDatakind() == 1) {
                        deaths = deaths + data.getQty();
                    }
                    if (data.getDatakind() == 3) {
                        confirmed = confirmed + data.getQty();
                    }
                    if (data.getDatakind() == 2) {
                        recovered = recovered + data.getQty();
                    }
                }
                list[i][0] = "<h2>" + countries.get(i).getName() + "</h2>" + "<p>" + date1 + " - " + date2 + "</p>"
                        + "<table><tr><th>Θάνατοι</th><th>Επιβεβ.</th><th>Ανέκαμψαν</th></tr>"
                        + "<tr><td align='center'>" + deaths + "</td><td align='center'>"
                        + confirmed + "</td><td align='center'>" + recovered + "</td></tr></table>";
                list[i][1] = String.valueOf(countries.get(i).getLat());
                list[i][2] = String.valueOf(countries.get(i).getLong1());
            }
        }
        String Lat = String.valueOf(countries.get(0).getLat());
        String Long = String.valueOf(countries.get(0).getLong1());
        FileWriter myWriter;
        try {
            myWriter = new FileWriter("src\\Form\\mappage.html");
            myWriter.write("<!DOCTYPE html>\n"
                    + "<html>\n"
                    + "<head>\n"
                    + "<title>Covid19 Stats Map</title>\n"
                    + "    <style type=\"text/css\">\n"
                    + "    #map {\n"
                    + "      height: 724px;\n"
                    + "      width: 100%;\n"
                    + "    }\n"
                    + "    table {border-collapse: collapse;}\n"
                    + "    table tr td, table tr th {border:1px solid #ccc; padding:5px;}\n"
                    + "  </style>\n"
                    + "<script>\n"
                    + "function initMap() {\n\n"
                    + "//The countries lat, long \n");
            for (int i = 0; i < countries.size(); i++) {
                myWriter.write("const country" + String.valueOf(i) + " = new google.maps.LatLng(" + list[i][1] + ", " + list[i][2] + ");\n");
            }
            myWriter.write("\n// The map \n"
                    + "const map = new google.maps.Map(document.getElementById(\"map\"), { \n"
                    + "zoom: 6, \n"
                    + "center: country0 \n"
                    + "});\n"
                    + "\n"
                    + "//Info Windows\n");
            for (int i = 0; i < countries.size(); i++) {
                myWriter.write("const infowindow" + String.valueOf(i) + " = new google.maps.InfoWindow({content:\"" + list[i][0] + "\"});\n");
            }
            myWriter.write("\n//Markers \n");
            for (int i = 0; i < countries.size(); i++) {
                myWriter.write("const marker" + String.valueOf(i) + " = new google.maps.Marker({position:country" + String.valueOf(i) + ", label:\"" + String.valueOf(i) + "\", animation: google.maps.Animation.DROP, map: map});\n");
            }
            myWriter.write("\n//Listeners \n");
            for (int i = 0; i < countries.size(); i++) {
                myWriter.write("marker" + String.valueOf(i) + ".addListener(\"click\", () => {infowindow" + String.valueOf(i) + ".open(map, marker" + String.valueOf(i) + ");});\n");
            }
            myWriter.write("\n//Auto fit zoom\n"
                    + "map.setCenter(new google.maps.LatLng(" + Lat + ", " + Long + "));\n"
                    + "var bounds = new google.maps.LatLngBounds();\n");
            for (int i = 0; i < countries.size(); i++) {
                myWriter.write("bounds.extend(country" + String.valueOf(i) + ");\n");
            }
            myWriter.write("google.maps.event.addListenerOnce(map, 'idle', function() {\n"
                    + "    fitAllBounds(bounds);\n"
                    + "});\n"
                    + "function fitAllBounds(b) {\n"
                    + "// Get north east and south west markers bounds coordinates\n"
                    + "var ne = b.getNorthEast();\n"
                    + "var sw = b.getSouthWest();\n"
                    + "// Get the current map bounds\n"
                    + "var mapBounds = map.getBounds();\n"
                    + "// Check if map bounds contains both north east and south west points\n"
                    + "if (mapBounds.contains(ne) && mapBounds.contains(sw)) {\n"
                    + "// Everything fits\n"
                    + "return;\n"
                    + "} else {\n"
                    + "var mapZoom = map.getZoom();\n"
                    + "if (mapZoom > 0) {\n"
                    + "// Zoom out\n"
                    + "map.setZoom(mapZoom - 1);\n"
                    + "// Try again\n"
                    + "fitAllBounds(b);\n"
                    + "}\n"
                    + "}\n"
                    + "}\n"
                    + "}\n"
                    + "</script>\n"
                    + "</head>\n"
                    + "<body>\n"
                    + "<h1 style=\"font-family:sans-serif\">Covid19 Stats Map</h1>\n"
                    + "<div id=\"map\"></div>\n"
                    + "<script src=\"https://maps.googleapis.com/maps/api/js?key=AIzaSyDCuNY4qQpwffF0ExviPM6zVYZ9_TCdgZE&callback=initMap&libraries=&v=weekly\""
                    + " async type=\"text/javascript\"></script>\n"
                    + "</body>\n"
                    + "</html>");
            myWriter.close();
        } catch (IOException ex) {
            System.out.println(ex);
        }

        String html = "src\\Form\\mappage.html";
        try {
            Runtime.getRuntime().exec("cmd /c start " + html);
        } catch (Exception ex) {
            System.out.println(ex);
        }
    }//GEN-LAST:event_jButton11ActionPerformed

    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton7ActionPerformed
        //Επιλογή ΧΑΡΤΗ στην φόρμα Country
        //Αποθηκεύεται η μια επιλογή του χρήστη
        String selectedCountry = (String) jComboBox1.getSelectedItem();
        //οι ημερομηνίες που έχει επιλέξει αντιγράφονται
        //Πατώντας ΧΑΡΤΗΣ ο χρήστης μεταβαίνει στην φόρμα MAP με συμπληρωμένες τις προηγούμενες επιλογές του
        Date startDate = jDateChooser1.getDate();
        Date endDate = jDateChooser2.getDate();
        jDateChooser3.setDate(startDate);
        jDateChooser4.setDate(endDate);
        // Εμφάνιση φόρμας MAP
        Home.setVisible(false);
        Import.setVisible(false);
        Country.setVisible(false);
        Map.setVisible(true);
        JOptionPane.showMessageDialog(null, "Για να δείτε το χάρτη με τη βασική χώρα πατήστε ΕΦΑΡΜΟΓΗ\n"
                +"Για να δείτε συγκριτικά με τη βασική χώρα αποτελέσματα,\n"
                +"επιλέξτε χώρες από τη λίστα και πατήστε ΕΦΑΡΜΟΓΗ");   
    }//GEN-LAST:event_jButton7ActionPerformed

    private void jCheckBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBox1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jCheckBox1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */

        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Home.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Home.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Home.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Home.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Home().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel Country;
    private javax.swing.JPanel Home;
    private javax.swing.JPanel Import;
    private javax.swing.JMenuBar MainMenuBar;
    private javax.swing.JPanel Map;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton11;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JButton jButton7;
    private javax.swing.JButton jButton8;
    private javax.swing.JButton jButton9;
    private javax.swing.JCheckBox jCheckBox1;
    private javax.swing.JCheckBox jCheckBox2;
    private javax.swing.JCheckBox jCheckBox3;
    private javax.swing.JComboBox<String> jComboBox1;
    private javax.swing.JComboBox<String> jComboBox3;
    private javax.swing.JComboBox<String> jComboBox5;
    private com.toedter.calendar.JDateChooser jDateChooser1;
    private com.toedter.calendar.JDateChooser jDateChooser2;
    private com.toedter.calendar.JDateChooser jDateChooser3;
    private com.toedter.calendar.JDateChooser jDateChooser4;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JLayeredPane jLayeredPane1;
    private javax.swing.JList<String> jList1;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JMenuItem jMenuItem5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTabbedPane jTabbedPane2;
    private javax.swing.JTable jTable1;
    private javax.swing.JTable jTable2;
    private javax.swing.JTable jTable3;
    // End of variables declaration//GEN-END:variables
}
